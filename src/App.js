import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Accueil from './pages/Accueil';
import RoutePrincipal from './routes/route';

function App() {
  return (
    <div>
      <BrowserRouter>
        <RoutePrincipal/>
      </BrowserRouter>
    </div>
  );
}

export default App;
