import React from 'react'
import { Route, Switch } from 'react-router'
import Accueil from '../pages/Accueil'
import SignIn from '../pages/Auth/SignIn'
import SignUp from '../pages/Auth/SignUp'

export default function RoutePrincipal() {
    return (
        <div>
            <Switch>
                <Route exact path="/" component={Accueil}/>
                <Route exact path="/signUp" component={SignUp}/>
            </Switch>
        </div>
    )
}
