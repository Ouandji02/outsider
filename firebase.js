// Import the functions you need from the SDKs you need
import firebase from 'firebase/compat/app'
import firestore from 'firebase/compat/firestore'
import auth from 'firebase/compat/auth'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDKrI8UKXf4a4A4Efo80fjBtwURqywudkg",
  authDomain: "ousider-a38fb.firebaseapp.com",
  projectId: "ousider-a38fb",
  storageBucket: "ousider-a38fb.appspot.com",
  messagingSenderId: "554289675043",
  appId: "1:554289675043:web:2853942fdf50be73406a96",
  measurementId: "${config.measurementId}"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig)
export default firebase